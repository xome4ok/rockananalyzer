import re
from bs4 import BeautifulSoup
import ijson
from pymongo import MongoClient
import rockanalyzer.artists as artists
import rockanalyzer.nltk_helper as h


client = MongoClient()
db = client['ru_rock']
collection = db['songs']


def import_(f):
    i = 0
    with open(f) as file:
        songs = ijson.items(file, 'item')
        for song in songs:
            artist = BeautifulSoup(song['artist']).getText()
            title = BeautifulSoup(song['title']).getText()
            lyrics = BeautifulSoup(re.sub(r'\<b\>.*\<\/b\>', '', song['lyrics'])).getText()
            try:
                collection.insert({'artist': artist, 'title': title, 'lyrics': lyrics, 'url': song['url']})
            except UnicodeEncodeError:
                print('error on inserting %s  by %s' % (title, artist))
            i += 1
            print('inserted %s songs' % i)
    print("I'm ready!")

#import_("/home/xome4ok/Документы/amdm_data.json")

# !!IMPORTANT: this must be done on collection with index created by:
#   db.amdm_songs.createIndex( {artist : 1, title : 1}, {unique : true} )


def copy_wo_dups(new_coll):
    dups_num = 0
    for song in collection.find():
        try:
            db[new_coll].insert_one(song)
        except:
            print(song["artist"], song["title"], sep="\t")
            dups_num += 1
    print("I have got rid of %s duplicates" % dups_num)

#copy_wo_dups("amdm_songs")

def store_ld_info(new_coll):
    for a in artists.list1:
        db[new_coll].insert_one(h.lexical_diversity_info(a, wo_stopwords=False))
        db[new_coll].insert_one(h.lexical_diversity_info(a))
        db[new_coll].insert_one(h.lexical_diversity_info(a, do_lemmatization=True))
        print('done', a)

#store_ld_info("ld_info")