# -*- coding: utf-8 -*-
from pprint import pprint
import re
from collections import defaultdict
import nltk
from nltk.corpus import stopwords
from pymongo import MongoClient
from rockanalyzer import artists
import pymorphy2

client = MongoClient()
songs = client['ru_rock']['amdm_songs']
info = client['ru_rock']['ld_info']
morph = pymorphy2.MorphAnalyzer()

# morph = pymorphy2.MorphAnalyzer()
# p = morph.parse('телемотовелофоторадиомонтёров')[0].normal_form
# print(p)


def remove_all_but_cyrillic(s):
    # TODO: design the method which wouldn't remove regular english words.
    return re.sub(r'[^а-яА-ЯёЁ]', ' ', s)


def tokenize(s):
    return nltk.Text(nltk.word_tokenize(s))


def lower_text(text):
    return [w.lower() for w in text]


def vocabulary(words):
    return sorted(set(words))


def normalize(x):
    return lower_text(tokenize(remove_all_but_cyrillic(x)))


def lemmatize(words):
    return [morph.parse(word)[0].normal_form for word in words]


def find_all_songs_by(artist):
    return songs.find({"artist": re.compile(re.escape(artist), re.IGNORECASE)})


def remove_stopwords(words):
    sw = stopwords.words('russian')
    sw += ['припев', 'вступление', 'проигрыш', 'раза']  # text-specific stopwords
    sw += ['наш', 'твой', 'свой', 'весь']  # incorrect POS tagging exceptions
    POSes = ['NPRO', 'PREP', 'CONJ', 'PRCL', 'INTJ']
    return [w for w in words if w not in sw and morph.parse(w)[0].tag.POS not in POSes]


def songs_number_by_artist():
    so = defaultdict(int)
    for artist in artists.list1:
        so[artist] = find_all_songs_by(artist).count()
    return sorted(so.items(), key=lambda x: x[1], reverse=True)


def words_in_each_song_by(artist):
    normal_songs = []
    for song in find_all_songs_by(artist):
        normal = normalize(song["lyrics"])
        minimum_words_in_song = 4
        if len(normal) > minimum_words_in_song:
            normal_songs.append(normal)
    return normal_songs


def songs_corpus_by(artist, wo_stopwords=True, do_lemmatization=False):
    from itertools import chain

    w = words_in_each_song_by(artist)
    song_words = list(chain.from_iterable(w))
    # if wo_stopwords:
    #    song_words = remove_stopwords(song_words)
    #  maybe try removing stopwords twice, to eliminate most of conjugated forms
    if do_lemmatization:
        song_words = lemmatize(song_words)
    if wo_stopwords:
        song_words = remove_stopwords(song_words)
    return {'songs_count': len(w), 'text': song_words}


def lexical_diversity_info(artist, wo_stopwords=True, do_lemmatization=False):
    corpus = songs_corpus_by(artist, wo_stopwords=wo_stopwords, do_lemmatization=do_lemmatization)
    vocab = vocabulary(corpus["text"])
    all_words_count = len(corpus['text'])
    return {'artist': artist,
            'songs_count': corpus['songs_count'],
            'all_words_count': all_words_count,
            'wo_stopwords': wo_stopwords,
            'lemmatization': do_lemmatization,
            'vocabulary': vocab,
            'LD': len(vocab) / all_words_count,  # lexical diversity, unique words count compared to all
            'WPS': all_words_count / corpus['songs_count'],  # words per song, avg.
            'UWPS': len(vocab) / corpus['songs_count']}  # unique words per song, avg.

