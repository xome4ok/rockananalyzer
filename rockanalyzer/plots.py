from collections import defaultdict
from pprint import pprint
from rockanalyzer import nltk_helper as h
from nltk import FreqDist
__author__ = 'xome4ok'
# import wordcloud # pretty lib, like to use it
import seaborn as sns
from matplotlib import pyplot as plt


def plot_most_common_by(artist, count=50, do_lemmatization=True):
    c = h.songs_corpus_by(artist, do_lemmatization=do_lemmatization)
    fd = FreqDist(c["text"])
    top = fd.most_common(count)

    sns.set_style("whitegrid")
    f , ax = plt.subplots()
    ax.set_xlabel(u"Количество")
    ax.set_title(u"%s - %s самых частотных слов" % (artist,count))

    ax = sns.barplot(y=[x for x,_ in top], x=[y for _,y in top], palette="Blues_d", ax=ax, orient='h')
    plt.setp(f.axes)
    plt.show()

#a = "Сплин"
#plot_most_common_by(a, count=20, do_lemmatization=True)

#result = defaultdict(dict)
#    for i in h.info.find({'wo_stopwords': True,'lemmatization': True}):
#        result[i['artist']] = {'songs': i['songs_count'], 'words': i['all_words_count'], 'LD': i['LD'], 'WPS': i['WPS'], 'UWPS': i['UWPS']}

def plot_db_word_stats(top=50,reverse=True):
    result = defaultdict(dict)
    for i in h.info.find({'wo_stopwords': True,'lemmatization': True}):
        result[i['artist']] = {'songs': i['songs_count'], 'words': i['all_words_count'], 'LD': i['LD'], 'WPS': i['WPS'], 'UWPS': i['UWPS']}
    #for item in sorted(result.items(), key=lambda x: x[1]['words'], reverse=True):
        #    print(item[0],item[1]['words'])

    sns.set_style("whitegrid")
    f , ax = plt.subplots()
    ax.set_xlabel(u"Words totally")
    ax.set_title(u"DB info: words statistics")
    s = sorted(result.items(), key=lambda x: x[1]['words'], reverse=reverse)[:top]
    ax = sns.pointplot(y=[x[0] for x in s], x=[y[1]['words'] for y in s], palette="Blues_d", ax=ax, orient='h')
    plt.setp(f.axes)
    plt.show()

# plot_db_word_stats(50)

def ld_stats(top=50): # TODO: think about how it should look to represent the right information
    result = defaultdict(dict)
    for i in h.info.find({'wo_stopwords': True,'lemmatization': True}):
        result[i['artist']] = {'songs': i['songs_count'], 'words': i['all_words_count'], 'LD': i['LD'], 'WPS': i['WPS'], 'UWPS': i['UWPS']}

    sns.set_style("whitegrid")
    f, ax = plt.subplots()
    ax.set_xlabel(u"LD")
    ax.set_title(u"Lexical diversity by artist (top 80 from top 100 by words count)")
    s1 = sorted(result.items(), key=lambda x: x[1]['words'], reverse=True)[:100]
    s = sorted(s1, key=lambda x: x[1]['LD'],reverse=True)[:top]
    ax = sns.pointplot(y=[x[0] + " " + str(x[1]['words']) for x in s], x=[y[1]['LD'] for y in s], palette="Blues_d", ax=ax, orient='h')
    plt.setp(f.axes)
    plt.show()

#ld_stats(top=80)

def print_sorted_by_ld(threshold):
    result = defaultdict(tuple)
    for i in h.info.find({'wo_stopwords': True,'lemmatization': False}):
        if i['all_words_count'] > threshold: # sayonara artists who have not enough songs
            result[i['artist']] = (len(i['vocabulary']),i['all_words_count'],i['LD'])

    pprint(sorted(result.items(),key=lambda x:x[1][2],reverse=True))
# print_sorted_by_ld(10000)